package decoder;

import com.formdev.flatlaf.FlatDarculaLaf;
import data.DataValidator;
import data.SampleDataModel;
import logging.ConfiguredLogger;
import popups.PopupTextPanel;
import styling.Styler;

import javax.swing.*;
import javax.swing.event.ChangeListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.function.Function;
import java.util.logging.Logger;

public class SampleSelectorBox extends Box {

    private static final Logger logger = ConfiguredLogger.getLogger("SampleSelectorBox");

    private static final int MAX_SLIDER_VALUE = 10_000;

    private static final int MIN_SLIDER_WIDTH = 200;

    /** === Enum Definitions ------------------------------------------------- **/

    private enum TimeUnit {
        SECOND("s", 1),
        MILLI_SECOND("ms", 1000),
        MICRO_SECOND("us", 1000_000),
        NANO_SECOND("ns", 1000_000_000);

        TimeUnit(String name, long seconds) {
            this.name = name;
            this.seconds = seconds;
        }

        @Override
        public String toString() {return this.name;}

        private final String name;
        private final long seconds;
    }


    /** === Interface Functions ---------------------------------------------- **/

    /**
     * Creates a sample selector box from the specified data snapshot.
     *
     * @param label The label to be displayed as a {@link String}.
     * @param snapshot The data snapshot as a {@link SampleDataModel.Snapshot}.
     *
     * @return The sample selector as a {@link SampleSelectorBox}.
     */
    public static SampleSelectorBox from(String label, SampleDataModel.Snapshot snapshot) {

        assert snapshot != null: "The specified snapshot must be non-null.";

        Double[] sampleTime = snapshot.getSampleTime();

        double rangeStart = sampleTime[0];
        double rangeEnd = sampleTime[snapshot.getSampleCount() - 1];
        double tickSize = snapshot.getSamplingPeriod();

        return new SampleSelectorBox(label, rangeStart, rangeEnd, tickSize);
    }

    /**
     * Creates a sample selector box from the specified data snapshot.
     *
     * @param snapshot The data snapshot as a {@link SampleDataModel.Snapshot}.
     *
     * @return The sample selector as a {@link SampleSelectorBox}.
     */
    public static SampleSelectorBox from(SampleDataModel.Snapshot snapshot) {
        return from("", snapshot);
    }

    /**
     * Validates the sample specified fro this {@link SampleSelectorBox} and displays an error message if the data
     * is invalid.
     *
     * @return True if a valid sample is specified, false otherwise.
     */
    public boolean validateData() {

        String validationResult = this.dataValidator.on(this.spinner.getValue().toString());

        if(validationResult == null) {
            return true;
        } else {
            PopupTextPanel.displayPopup(this, validationResult,
                    PopupTextPanel.PopupType.ERROR, PopupTextPanel.PopupLocation.RIGHT, true);
            return false;
        }
    }

    /**
     * @return The currently set time value for this {@link SampleSelectorBox} in seconds.
     */
    public double getValue() {

        assert validateData(): "The data specified for this sample selector is invalid.";

        double currentValue = this.sampleRounder.apply((double) this.spinner.getValue());
        return currentValue / ((TimeUnit) Objects.requireNonNull(this.timeUnitBox.getSelectedItem())).seconds;
    }

    /**
     * Sets the specified value for this {@link SampleSelectorBox}.
     *
     * @param newValue The new time value (in seconds).
     */
    public void setValue(double newValue) {

        assert validateData(): "The data specified for this sample selector is invalid.";
        this.spinner.setValue(this.sampleRounder.apply(
                newValue * ((TimeUnit) Objects.requireNonNull(this.timeUnitBox.getSelectedItem())).seconds));
    }

    /**
     * Updates the data snapshot for this {@link SampleSelectorBox}.
     *
     * @param newSnapshot The new data snapshot as a {@link SampleDataModel.Snapshot}.
     */
    public void updateSnapshot(SampleDataModel.Snapshot newSnapshot) {

        assert newSnapshot != null: "The specified snapshot must be non-null.";

        Double[] sampleTime = newSnapshot.getSampleTime();

        this.rangeStart = sampleTime[0];
        this.rangeEnd = sampleTime[newSnapshot.getSampleCount() - 1];
        this.tickSize = newSnapshot.getSamplingPeriod();
    }

    /**
     * Adds the specified action listener to this {@link SampleSelectorBox}.
     *
     * @param actionListener The action listener as a {@link ActionListener}.
     */
    public void addActionListener(ActionListener actionListener) {
        this.actionListeners.add(actionListener);
    }

    /**
     * Removes the specified action listener from this {@link SampleSelectorBox}.
     *
     * @param actionListener The action listener as a {@link ActionListener}.
     */
    public void removeActionListener(ActionListener actionListener) {
        this.actionListeners.remove(actionListener);
    }

    /**
     * Links this {@link SampleSelectorBox} with the specified one. Linked selectors will always have the same time
     * unit.
     *
     * @param selectorBox The selector to be linked as a {@link SampleSelectorBox}.
     */
    public void linkSampleSelectorBox(SampleSelectorBox selectorBox) {
        this.linkedSelectors.add(selectorBox);
    }

    /**
     * Enables/Disables this {@link SampleSelectorBox} and all of it's interactive components.
     *
     * @param isEnabled If True this sample selector will be enabled, otherwise it will be disabled.
     */
    @Override
    public void setEnabled(boolean isEnabled) {
        List.of(this.slider, this.spinner, this.timeUnitBox).forEach(component -> component.setEnabled(isEnabled));
    }


    /** === Main ------------------------------------------------------------- **/

    public static void main(String[] args) {

        FlatDarculaLaf.setup();

        try {
            UIManager.setLookAndFeel(new FlatDarculaLaf());
        } catch (Exception ignored) {}

        JFrame testFrame = new JFrame();
        testFrame.getContentPane().add(new SampleSelectorBox("Test Box", 0.0005, 10, 0.00001));

        testFrame.pack();
        testFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        testFrame.setVisible(true);
    }


    /** === Private Methods -------------------------------------------------- **/

    /**
     * Creates a sample selector box from the specified parameters.
     *
     * @param label The label to be displayed as a {@link String}.
     * @param rangeStart The start time of the sample range (in seconds).
     * @param rangeEnd The end time of the sample range (in seconds).
     * @param tickSize The minimum time increment (in seconds).
     */
    private SampleSelectorBox(String label, double rangeStart, double rangeEnd, double tickSize) {

        super(BoxLayout.X_AXIS);

        assert rangeStart <= rangeEnd: "The specified range (" + rangeStart + ", " + rangeEnd + ") is not well formed.";

        this.actionListeners = new CopyOnWriteArrayList<>();
        this.linkedSelectors = new CopyOnWriteArrayList<>();

        this.label = label;
        this.rangeStart = Math.round(rangeStart / tickSize) * tickSize;
        this.rangeEnd = Math.round(rangeEnd / tickSize) * tickSize;
        this.tickSize = tickSize;

        this.createComponents();
        this.initializeModel();
        this.addComponents();
    }

    /**
     * Creates the GUI components for this {@link SampleSelectorBox}.
     */
    private void createComponents() {

        this.slider = new JSlider();
        this.spinner = new JSpinner();
        this.timeUnitBox = new JComboBox<>(TimeUnit.values());

        this.slider.setMinimumSize(new Dimension(MIN_SLIDER_WIDTH, 0));

        this.timeUnitBox.setSelectedItem(TimeUnit.SECOND);
        this.timeUnitBox.addActionListener(actionEvent -> this.initializeModel());

        this.timeUnitBox.addActionListener(this.timeUnitLinker);

        this.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseEntered(MouseEvent e) {PopupTextPanel.removePopup(SampleSelectorBox.this);}
        });

        this.slider.addChangeListener(changeEvent -> PopupTextPanel.removePopup(SampleSelectorBox.this));
        this.spinner.addChangeListener(changeEvent -> PopupTextPanel.removePopup(SampleSelectorBox.this));
        this.timeUnitBox.addActionListener(actionEvent -> PopupTextPanel.removePopup(SampleSelectorBox.this));
    }

    /**
     * Initializes the underlying model for this {@link SampleSelectorBox}.
     */
    private synchronized void initializeModel() {

        this.slider.removeChangeListener(this.sliderChangeListener);
        this.spinner.removeChangeListener(this.spinnerChangeListener);

        final long TIME_MULTIPLIER = ((TimeUnit) Objects.requireNonNull(this.timeUnitBox.getSelectedItem())).seconds;

        final double RANGE_START = this.rangeStart * TIME_MULTIPLIER;
        final double RANGE_END = this.rangeEnd * TIME_MULTIPLIER;
        final double TICK_SIZE = this.tickSize * TIME_MULTIPLIER;
        final double RANGE = RANGE_END - RANGE_START;

        SpinnerNumberModel model = new SpinnerNumberModel();
        model.setMinimum(RANGE_START);
        model.setMaximum(RANGE_END);
        model.setStepSize(TICK_SIZE);
        model.setValue(RANGE_START);

        this.sampleRounder = value -> Math.floor(value / TICK_SIZE) * TICK_SIZE;

        this.dataValidator = data -> {

            double dataValue;

            try {
                dataValue = Double.parseDouble(data);
            } catch (NumberFormatException e) {
                return "The specified sample timestamp must be a number.";
            }

            if(dataValue < RANGE_START || RANGE_END < dataValue) {
                return dataValue + " is not a valid value in the range (" + RANGE_START + ", " + RANGE_END + ").";
            } else {
                return null;
            }
        };

        this.sliderChangeListener = changeEvent -> {

            double sliderValue = this.slider.getValue();
            double calculatedSpinnerValue =
                    this.sampleRounder.apply(RANGE_START + (sliderValue * RANGE / MAX_SLIDER_VALUE));

            this.spinner.removeChangeListener(this.spinnerChangeListener);
            this.spinner.setValue(calculatedSpinnerValue);
            this.spinner.addChangeListener(this.spinnerChangeListener);

            this.triggerActionListeners();
        };

        this.spinnerChangeListener = changeEvent -> {

            double spinnerValue = (double) this.spinner.getValue();
            int calculatedSliderValue = (int) ((spinnerValue - RANGE_START) * MAX_SLIDER_VALUE/RANGE);

            this.slider.removeChangeListener(this.sliderChangeListener);
            this.slider.setValue(calculatedSliderValue);
            this.slider.addChangeListener(this.sliderChangeListener);

            this.triggerActionListeners();
        };

        this.spinner.setModel(model);

        this.slider.setMinimum(0);
        this.slider.setMaximum(MAX_SLIDER_VALUE);
        this.slider.setValue(0);

        this.slider.addChangeListener(this.sliderChangeListener);
        this.spinner.addChangeListener(this.spinnerChangeListener);

        this.triggerActionListeners();
    }

    /**
     * Adds the GUI components to this {@link SampleSelectorBox}.
     */
    private void addComponents() {

        this.add(Styler.style(new JLabel(this.label)));
        this.add(Box.createHorizontalGlue());
        this.add(this.slider);
        this.add(Styler.style(this.spinner));
        this.add(Styler.style(this.timeUnitBox));
    }

    /**
     * Triggers an action event for all action listeners registered to this {@link SampleSelectorBox}.
     */
    private void triggerActionListeners() {

        ActionEvent actionEvent = new ActionEvent(this, ActionEvent.ACTION_PERFORMED, "");
        this.actionListeners.forEach(actionListener -> actionListener.actionPerformed(actionEvent));
    }

    /**
     * @return A {@link List} of all {@link SampleSelectorBox}es linked to this one.
     */
    private List<SampleSelectorBox> getLinkedSelectors() {return this.linkedSelectors;}


    /** === Instance Variables ----------------------------------------------- **/

    private final List<ActionListener> actionListeners;
    private final List<SampleSelectorBox> linkedSelectors;

    private final String label;
    private double rangeStart;
    private double rangeEnd;
    private double tickSize;

    private JSlider slider;
    private JSpinner spinner;
    private JComboBox<TimeUnit> timeUnitBox;

    private ChangeListener sliderChangeListener;
    private ChangeListener spinnerChangeListener;

    private Function<Double, Double> sampleRounder;
    private DataValidator dataValidator;

    /**
     * Adjusts the time units of all sample selectors linked to this one.
     */
    private final ActionListener timeUnitLinker = actionEvent -> {

        this.getLinkedSelectors().forEach(selectorBox -> {

            selectorBox.timeUnitBox.removeActionListener(selectorBox.timeUnitBox);
            selectorBox.timeUnitBox.setSelectedItem(this.timeUnitBox.getSelectedItem());
            selectorBox.timeUnitBox.addActionListener(selectorBox.timeUnitBox);
        });
    };
}
