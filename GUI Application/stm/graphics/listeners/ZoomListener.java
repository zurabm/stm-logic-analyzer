package listeners;

public interface ZoomListener {

    /**
     * Invoked whenever a user requests to zoom on a plot panel displaying sample information by dragging the
     * mouse across the chosen samples.
     *
     * @param startIndex The index of first sample chosen by the user.
     * @param endIndex The index of last sample chosen by the user.
     */
    void zoomRequested(int startIndex, int endIndex);
}
