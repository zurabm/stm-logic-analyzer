package channel;

import logging.ConfiguredLogger;
import utility.Sample;

import javax.swing.*;
import java.awt.*;
import java.util.Arrays;
import java.util.logging.Logger;

import static styling.GUIConstants.PLOT_LINE_COLOR;
import static styling.GUIConstants.SAMPLES_PER_PIXEL;

/**
 * Class: SampleView
 * Date Created: 11.07.21 22:38
 *
 * === Description ============================================= *
 * SampleView is a utility class for displaying a set of samples on a {@link PlotPanel}.
 */
public class SampleView {

    private final Logger logger = ConfiguredLogger.getLogger("SampleView");

    /** === Constant Definitions --------------------------------------------- **/

    public static final int DEFAULT_WIDTH = 500;
    public static final int DEFAULT_HEIGHT = 150;

    private static final boolean DEBUG = false;
    private static final Color DEBUG_COLOR = Color.BLUE;


    /** === Interface Functions ---------------------------------------------- **/

    /**
     * Creates a default {@link SampleView}.
     */
    public SampleView() {
        this(DEFAULT_WIDTH, DEFAULT_HEIGHT);
    }

    /**
     * Creates a sample view of the specified dimensions.
     *
     * @param width The width of the sample view.
     * @param height The height of the sample view.
     */
    public SampleView(int width, int height) {
        this(width, height, new Sample[]{});
    }

    /**
     * Creates a sample view of the specified dimensions, displaying the given
     * array of samples.
     *
     * @param width The width of the sample view.
     * @param height The height of the sample view.
     * @param samples The samples as an array of {@link Sample}s.
     */
    public SampleView(int width, int height, Sample[] samples) {

        this.width = width;
        this.height = height;

        this.xOffset = 0;
        this.yOffset = 0;

        this.setSamples(samples);
    }

    /**
     * @return The number of samples currently being displayed by this {@link SampleView}.
     */
    public int getSampleCount() {return this.samples.length;}

    /**
     * @return The minimum valued x component of the currently set samples.
     */
    public double getMinX() {return this.minX;}

    /**
     * @return The maximum valued x component of the currently set samples.
     */
    public double getMaxX() {return this.maxX;}

    /**
     * @return The minimum valued y component of the currently set samples.
     */
    public double getMinY() {return this.minY;}

    /**
     * @return The maximum valued y component of the currently set samples.
     */
    public double getMaxY() {return this.maxY;}

    /**
     * Returns the (absolute) index of sample with the x value closest to the one specified.
     *
     * @param x The target value as a double.
     *
     * @return The index of the sample with the closest x value.
     */
    public int getClosestSampleIndex(double x) {

        double minX = this.getMinX();
        double maxX = this.getMaxX();

        if(!(minX <= x && x <= maxX)) {
            logger.severe("The specified x value is out of range for the current samples.\n" +
                    "Specified: " + x + ", Sample Range: (" + minX + ", " + maxX + ").");
            return -1;
        }

        int lowerBound = 0;
        int upperBound = this.samples.length;

        while(lowerBound < upperBound) {

            int middle = (lowerBound + upperBound)/2;
            int comparisonValue = Double.compare(this.samples[middle].getX(), x);

            if(comparisonValue == 0) return middle;
            else if(comparisonValue < 0) lowerBound = middle + 1;
            else upperBound = middle;
        }

        return this.sampleIndexOffset + lowerBound;
    }

    /**
     * Returns the sample with the x value closest to the one specified.
     *
     * @param x The target value as a double.
     *
     * @return The sample with the closest x value as a {@link Sample}.
     */
    public Sample getClosestSample(double x) {

        int index = this.getClosestSampleIndex(x);

        if(index == -1) return null;
        return this.originalSamples[index];
    }

    /**
     * Sets the sample set of the sample view to the specified array of samples.
     *
     * @param samples The samples as an array of {@link Sample} objects.
     */
    public void setSamples(Sample[] samples) {
        this.setSamples(samples, false);
    }

    /**
     * Sets the sample set of the sample view to the specified array of samples.
     *
     * @param samples The samples as an array of {@link Sample} objects.
     * @param isZoom If false, a reference to the sample array is saved for future use.
     */
    private void setSamples(Sample[] samples, boolean isZoom) {

        synchronized (this) {

            if(!isZoom) {
                this.originalSamples = samples;
            } else {
                this.sampleIndexOffset = 0;
            }
            this.samples = samples;
        }

        this.calculateExtremeValues();
        this.calculateNormalizationRatios();
    }

    /**
     * Sets the dimensions of the sample view.
     *
     * @param width The new width for the sample view.
     * @param height The new height for the sample view.
     */
    public void setSize(int width, int height) {

        this.width = width;
        this.height = height;
        this.calculateNormalizationRatios();
    }

    /**
     * Sets the offset of this sample view.
     *
     * @param xOffset The margin from the left edge of the graphics.
     * @param yOffset The margin from the top edge of the graphics.
     */
    public void setOffsets(int xOffset, int yOffset) {

        this.xOffset = xOffset;
        this.yOffset = yOffset;
    }

    /**
     * Displays the current set of samples on the specified graphics.
     *
     * @param graphics The graphics to be painted as a {@link Graphics} object.
     */
    public void displaySamples(Graphics graphics) {

        Color original = graphics.getColor();
        graphics.setColor(PLOT_LINE_COLOR);

        Sample previousSample = null;

        for(Sample currentSample : this.getNormalizedSamples()) {

            if(previousSample != null) {

                graphics.drawLine((int) previousSample.getX(), (int) previousSample.getY(), (int) currentSample.getX(), (int) previousSample.getY());
                graphics.drawLine((int) currentSample.getX(), (int) previousSample.getY(), (int) currentSample.getX(), (int) currentSample.getY());
            }

            previousSample = currentSample;
        }

        if(DEBUG) {

            graphics.setColor(DEBUG_COLOR);
            graphics.drawString("Center", this.width/2, this.height/2);
            graphics.drawRect(this.xOffset, this.yOffset, this.width, this.height);
        }

        graphics.setColor(original);
    }

    /**
     * Zooms the currently displayed plot panel to show all samples in the specified range.
     *
     * @param startIndex The start of the range as the index of the first sample to be included.
     * @param endIndex The end of the range as the index of last last to be included.
     */
    public void zoom(int startIndex, int endIndex) {

        assert this.originalSamples.length > 0: "Samples are not specified for this sample view.";

        assert startIndex <= endIndex: "The zooming range is not well formed: (" + startIndex + ", " + endIndex + ").";

        int newLength = endIndex - startIndex + 1;
        Sample[] samples = new Sample[newLength];

        System.arraycopy(this.originalSamples, startIndex, samples, 0, newLength);

        this.setSamples(samples, true);
        this.sampleIndexOffset = startIndex;
    }

    /**
     * Resets this {@link SampleView} to it's original zoom state.
     */
    public void resetZoom() {
        this.setSamples(this.originalSamples);
    }


    /** === Private Functions ------------------------------------------------ **/

    /**
     * Calculates the extreme values of the samples.
     */
    private void calculateExtremeValues() {

        this.minX = Arrays.stream(this.samples).mapToDouble(Sample::getX).min().orElse(0);
        this.maxX = Arrays.stream(this.samples).mapToDouble(Sample::getX).max().orElse(0);
        this.minY = Arrays.stream(this.samples).mapToDouble(Sample::getY).min().orElse(0);
        this.maxY = Arrays.stream(this.samples).mapToDouble(Sample::getY).max().orElse(0);
    }

    /**
     * Calculates the normalization ratios for the current sample set and dimensions.
     */
    private void calculateNormalizationRatios() {

        double rangeX = this.maxX - this.minX;
        double rangeY = this.maxY - this.minY;

        this.xNormalizationRatio = this.width / rangeX;
        this.yNormalizationRatio = this.height / rangeY;
    }

    /**
     * @return An array of normalized values for the current sample set.
     */
    private Sample[] getNormalizedSamples() {

        Sample[] normalizedSamples;

        synchronized (this) {

            int nSamples = Math.min(this.samples.length, this.width * SAMPLES_PER_PIXEL);
            double sampleIncrement = (nSamples == 0 ? 0 : (double) this.samples.length / nSamples);

            normalizedSamples = new Sample[nSamples];

            for (int i = 0; i < nSamples; i++) {

                Sample sample = this.samples[(int) (sampleIncrement * i)];

                int normalizedX = (int) ((sample.getX() - this.minX) * this.xNormalizationRatio);
                int normalizedY = (int) ((sample.getY() - this.minY) * this.yNormalizationRatio);

                normalizedSamples[i] = new Sample(this.xOffset + normalizedX, this.yOffset + height - normalizedY);
            }
        }
        return normalizedSamples;
    }

    /** === Instance Variables ----------------------------------------------- **/

    private int width, height;
    private int xOffset, yOffset;
    private double minX, minY, maxX, maxY;
    private double xNormalizationRatio, yNormalizationRatio;

    private int sampleIndexOffset;

    private Sample[] samples;
    private Sample[] originalSamples;
}
