package channel;

import buttons.Button;
import buttons.LinkedButtonPanel;
import buttons.ToggleButton;
import configuration.ChannelConfiguration;
import listeners.ChannelReorderingListener;
import listeners.TriggerSelectionListener;
import listeners.ZoomListener;
import logging.ConfiguredLogger;
import popups.PopupTextPanel;
import styling.Styler;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.util.List;
import java.util.*;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static data.DataConstants.N_CHANNELS;
import static styling.GUIConstants.*;

public class Channel {

    private static final Logger logger = ConfiguredLogger.getLogger("Channel");

    /** === Interface Functions ---------------------------------------------- **/

    /**
     * @return The name of this {@link Channel}.
     */
    public String getChannelName() {
        return this.channelName;
    }

    /**
     * @return The index of this channel as an integer.
     */
    public int getChannelIndex() {return this.channelIndex;}

    /**
     * @return Returns the control panel part of this channel as a {@link JPanel}.
     */
    public JPanel getControlPanel() {return this.control;}

    /**
     * @return Returns the display panel part of this channel as a {@link JPanel}.
     */
    public JPanel getDisplayPanel() {return this.display;}

    /**
     * @return Returns the currently active trigger mode for this channel.
     */
    public synchronized ChannelConfiguration.Trigger getTrigger() {
        return this.trigger;
    }

    /**
     * @return True if this channel has been selected for sampling.
     */
    public boolean isSelected() {return this.isSelected;}

    /**
     * @return True if this channel is collapsed.
     */
    public boolean isCollapsed() {
        return this.isCollapsed;
    }

    /**
     * @return The current configuration of this channel as a {@link ChannelConfiguration} object.
     */
    public ChannelConfiguration getConfiguration() {

        return new ChannelConfiguration(
            this.getChannelIndex(),
            this.getTrigger()
        );
    }

    /**
     * Sets a new width for the display part of this channel.
     *
     * @param newWidth The new width of the display.
     */
    public void setDisplayWidth(int newWidth) {

        this.displayPanelWidth = newWidth;
        this.setCollapsed(this.isCollapsed());
    }

    /**
     * Sets a new display name for this channel.
     *
     * @param newChannelName The new channel name.
     */
    public void setChannelName(String newChannelName) {

        this.channelName = newChannelName;
        this.channelNameLabel.setText(this.wrapChannelName(newChannelName));
    }

    /**
     * Sets the channel listening mode. If the channel is selected the data at the corresponding channel
     * of the STM will be sampled.
     *
     * @param isSelected The new listening mode for the channel.
     */
    public void setSelected(boolean isSelected) {

        this.isSelected = isSelected;

        if(this.channelSelected.isSelected() != isSelected) {
            this.channelSelected.setSelected(isSelected);
        }
    }

    /**
     * Clears all the visible samples on this channel.
     *
     * @param isIndeterminate If true the progress bar will be set to an indeterminate state.
     */
    public void clearDisplay(boolean isIndeterminate) {

        this.plotPanel.plot(new Number[0], new Number[0]);

        this.samplingProgress.setValue(0);
        this.samplingProgress.setIndeterminate(isIndeterminate);
        this.samplingProgress.setStringPainted(false);
        this.samplingProgress.setEnabled(false);

        this.isCleared = true;
    }

    /**
     * @return True if there is no sample information currently present on this channel's display.
     */
    public boolean isCleared() {return this.isCleared;}

    /**
     * Displays the specified samples on the channel's display area.
     *
     * @param sampleTime The time values of the samples.
     * @param sampleValue The logical values of the samples.
     */
    public void displaySamples(Double[] sampleTime, Byte[] sampleValue) {

        this.isCleared = false;

        if (this.samplingProgress.isEnabled()) this.updateSamplingProgress(sampleTime.length);

        this.plotPanel.plot(sampleTime, sampleValue, PlotPanel.SECONDS, PlotPanel.NO_UNIT);
    }

    /**
     * Zooms the plot panel of this channel to show all samples in the specified range.
     *
     * @param startIndex The start of the range as the index of the first sample to be included.
     * @param endIndex The end of the range as the index of last last to be included.
     */
    public void zoom(int startIndex, int endIndex) {
        this.plotPanel.zoom(startIndex, endIndex);
    }

    /**
     * Reverts any zooming done on the channel.
     */
    public void resetZoom() {
        this.plotPanel.resetZoom();
    }

    /**
     * Registers the specified action listener to the remove button of this channel.
     *
     * @param removeButtonListener The listener as an {@link ActionListener} object.
     */
    public void addRemoveButtonListener(ActionListener removeButtonListener) {
        this.removeButton.addActionListener(removeButtonListener);
    }

    /**
     * Registers the specified action listener to the edit button of this channel.
     *
     * @param editButtonListener The listener as an {@link ActionListener} object.
     */
    public void addEditButtonListener(ActionListener editButtonListener) {
        this.editButton.addActionListener(editButtonListener);
    }

    /**
     * Registers the specified action listener to the Select Check Box of this channel.
     *
     * @param channelSelectedListener The listener as an {@link ActionListener} object.
     */
    public void addChannelSelectedListener(ActionListener channelSelectedListener) {
        this.channelSelected.addActionListener(channelSelectedListener);
    }

    /**
     * Adds the specified trigger selection listener to this channel, which will be invoked whenever a trigger is
     * selected.
     *
     * @param triggerSelectionListener The {@link TriggerSelectionListener} object.
     */
    public void addTriggerSelectionListener(TriggerSelectionListener triggerSelectionListener) {

        synchronized (this.triggerSelectionListeners) {
            this.triggerSelectionListeners.add(triggerSelectionListener);
        }
    }

    /**
     * Adds the specified channel reordering listener to this channel, for which the appropriate method will be invoked
     * whenever the channel reqsuests to be reordered.
     *
     * @param channelReorderingListener The {@link ChannelReorderingListener} object.
     */
    public void addChannelReorderingListener(ChannelReorderingListener channelReorderingListener) {

        synchronized (this.channelReorderingListeners) {
            this.channelReorderingListeners.add(channelReorderingListener);
        }
    }

    /**
     * Adds the specified zoom listener to this {@link PlotPanel}.
     *
     * @param listener The zoom listener as a {@link ZoomListener}.
     */
    public void addZoomListener(ZoomListener listener) {
        this.plotPanel.addZoomListener(listener);
    }

    /**
     * Removes the specified zoom listener from this {@link PlotPanel}.
     *
     * @param listener The zoom listener as a {@link ZoomListener}.
     */
    public void removeZoomListener(ZoomListener listener) {
        this.plotPanel.removeZoomListener(listener);
    }

    /**
     * Displays the specified warning text near the trigger selection panel of this channel.
     *
     * @param text The text to be displayed to the user.
     */
    public void displayTriggerSelectionWarning(String text) {
        PopupTextPanel.displayPopup(this.triggerButtons, text, PopupTextPanel.PopupType.WARNING, PopupTextPanel.PopupLocation.RIGHT, true);
    }

    /**
     * Hides the warning displayed on the trigger selection panel of this channel.
     */
    public void hideTriggerSelectionWarning() {
        PopupTextPanel.removePopup(this.triggerButtons);
    }

    /**
     * enables/Disables the action components of this channel.
     *
     * @param isEnabled If true the action components will be enabled.
     */
    public void setEnabled(boolean isEnabled) {

        List.of(
            this.removeButton,
            this.editButton,
            this.channelSelected,
            this.triggerButtons,
            this.reorderUpButton,
            this.reorderDownButton
        ).forEach(component -> component.setEnabled(isEnabled));

        this.setReorderable(this.isFirst, this.isLast);
    }

    /**
     * Sets whether this channel can be reordered in a particular direction.
     *
     * @param isFirst If true the channel cannot be reordered up.
     * @param isLast If true the channel cannot be reordered down.
     */
    public void setReorderable(boolean isFirst, boolean isLast) {

        this.isFirst = isFirst;
        this.isLast = isLast;

        this.reorderUpButton.setEnabled(!isFirst);
        this.reorderDownButton.setEnabled(!isLast);
    }

    /**
     * Sets the sampling progress bar limit for this channel.
     *
     * @param nSamples The number of expected samples to be displayed on this channel.
     */
    public void setSamplingProgressLimit(int nSamples) {
        this.samplingProgress.setMaximum(nSamples);
    }

    /**
     * Enables the sampling progress bar for this channel.
     */
    public void enableSamplingProgress() {
        this.samplingProgress.setEnabled(true);
    }

    /**
     * Disables the sampling progress bar for this channel.
     */
    public void disableSamplingProgress() {
        this.samplingProgress.setEnabled(false);
        this.samplingProgress.setIndeterminate(false);
    }

    /**
     * Channel objects are equal if their indexes are equal. Additionally,
     * a Channel is equal to an Integer if the value of the Integer is the index of the channel.
     *
     * @param other Object to compare this channel to
     * @return True if Objects are equal, false otherwise.
     */
    @Override
    public boolean equals(Object other) {

        if(other instanceof Integer) return this.channelIndex == ((Integer) other);
        else return this.channelIndex == ((Channel) other).channelIndex;
    }

    /**
     * @return A human-readable representation of this {@link Channel}.
     */
    @Override
    public String toString() {
        return this.getChannelName() + " (" + this.getChannelIndex() + ")";
    }

    /** === Private Methods ----------------------------------------------- **/

    /**
     * Creates a new {@link Channel} object with the specified name and index.
     *
     * @param channelIndex The index of the channel.
     * @param channelName The name of the channel as a {@link String}.
     */
    private Channel(int channelIndex, String channelName) {

        this.channelName = channelName;
        this.channelIndex = channelIndex;
        this.isCleared = true;

        this.control = createControlPanel();
        this.display = createDisplayPanel();

        this.triggerSelectionListeners = new ArrayList<>();
        this.channelReorderingListeners = new ArrayList<>();

        this.setCollapsed(false);
        this.setTrigger(ChannelConfiguration.Trigger.NONE);
    }

    /**
     * Creates the header for the control panel.
     *
     * @param channelName The channel name to be displayed on the header.
     * @return The header as a {@link JPanel}.
     */
    private JPanel createControlPanelHeader(String channelName) {

        JPanel headerPanel = new JPanel();
        headerPanel.setLayout(new BoxLayout(headerPanel, BoxLayout.X_AXIS));

        Styler.setSize(headerPanel, CHANNEL_CONTROL_PANEL_WIDTH, CHANNEL_HEADER_HEIGHT);

        removeButton = new Button(REMOVE_BUTTON_ICONS);
        removeButton.setContentAreaFilled(false);

        editButton = new Button(EDIT_BUTTON_ICONS);
        editButton.setContentAreaFilled(false);

        channelNameLabel = new JLabel(this.wrapChannelName(channelName));

        channelSelected = new JCheckBox("Listen");
        channelSelected.addActionListener(actionEvent -> this.setSelected(channelSelected.isSelected()));

        headerPanel.add(removeButton);
        headerPanel.add(editButton);
        headerPanel.add(Box.createRigidArea(new Dimension(CHANNEL_HEADER_GAP, CHANNEL_HEADER_GAP)));
        headerPanel.add(channelNameLabel);
        headerPanel.add(Box.createRigidArea(new Dimension(CHANNEL_HEADER_GAP, CHANNEL_HEADER_GAP)));
        headerPanel.add(Box.createHorizontalGlue());
        headerPanel.add(channelSelected);
        headerPanel.add(Box.createRigidArea(new Dimension(CHANNEL_HEADER_GAP, CHANNEL_HEADER_GAP)));
        headerPanel.add(createReorderingButtons());

        Styler.style(headerPanel, Styler.Style.CHANNEL_HEADER);

        return headerPanel;
    }

    /**
     * Creates a panel with vertically placed reordering buttons.
     *
     * @return The button panel as a {@link JPanel} object.
     */
    private JPanel createReorderingButtons() {

        JPanel buttonPanel = new JPanel();
        buttonPanel.setLayout(new BoxLayout(buttonPanel, BoxLayout.Y_AXIS));

        this.reorderUpButton = new Button(MOVE_UP_BUTTON_ICONS);
        this.reorderDownButton = new Button(MOVE_DOWN_BUTTON_ICONS);

        this.reorderUpButton.setContentAreaFilled(false);
        this.reorderDownButton.setContentAreaFilled(false);

        this.reorderUpButton.addActionListener(actionEvent -> {
            synchronized (this.channelReorderingListeners) {
                this.channelReorderingListeners.forEach(listener -> listener.channelReorderedUp(this));
            }
        });

        this.reorderDownButton.addActionListener(actionEvent -> {
            synchronized (this.channelReorderingListeners) {
                this.channelReorderingListeners.forEach(listener -> listener.channelReorderedDown(this));
            }
        });

        buttonPanel.add(Box.createVerticalGlue());
        buttonPanel.add(reorderUpButton);
        buttonPanel.add(reorderDownButton);
        buttonPanel.add(Box.createVerticalGlue());

        return Styler.style(buttonPanel, Styler.Style.BORDERLESS);
    }

    /**
     * Creates the header for the display panel.
     *
     * @return The header as a {@link JPanel}.
     */
    private JPanel createDisplayPanelHeader() {

        JPanel headerPanel = new JPanel();
        headerPanel.setLayout(new BoxLayout(headerPanel, BoxLayout.X_AXIS));

        Styler.setSize(headerPanel, this.displayPanelWidth, CHANNEL_HEADER_HEIGHT);

        ToggleButton collapseButton = new ToggleButton(COLLAPSE_BUTTON_DISABLED_ICONS, COLLAPSE_BUTTON_ENABLED_ICONS);
        collapseButton.setAlignmentX(Component.LEFT_ALIGNMENT);
        collapseButton.setContentAreaFilled(false);

        collapseButton.addActionListener(actionEvent -> this.setCollapsed(!this.isCollapsed));

        this.samplingProgress = new JProgressBar();
        this.samplingProgress.setEnabled(false);
        Styler.setSize(this.samplingProgress, SAMPLING_PROGRESS_BAR_WIDTH, SAMPLING_PROGRESS_BAR_HEIGHT);

        headerPanel.add(Box.createRigidArea(new Dimension(SMALL_COMPONENT_GAP, SMALL_COMPONENT_GAP)));
        headerPanel.add(collapseButton);
        headerPanel.add(Box.createRigidArea(new Dimension(GENERIC_COMPONENT_GAP, GENERIC_COMPONENT_GAP)));
        headerPanel.add(this.samplingProgress);
        headerPanel.add(Box.createHorizontalGlue());

        Styler.style(headerPanel, Styler.Style.CHANNEL_HEADER);

        return headerPanel;
    }

    /**
     * Creates the control panel for this channel.
     *
     * @return The control panel as a {@link JPanel}.
     */
    private JPanel createControlPanel() {

        JPanel controlPanel = new JPanel();
        controlPanel.setLayout(new BoxLayout(controlPanel, BoxLayout.Y_AXIS));
        controlPanel.setBorder(BorderFactory.createEtchedBorder());

        // Add Trigger Buttons.
        JPanel triggerPanel = new JPanel();
        triggerPanel.setLayout(new BoxLayout(triggerPanel, BoxLayout.X_AXIS));
        triggerPanel.setBorder(BorderFactory.createEtchedBorder());

        ToggleButton risingButton = new ToggleButton(TRIGGER_RISING_DISABLED_BUTTON_ICONS, TRIGGER_RISING_ENABLED_BUTTON_ICONS);
        ToggleButton highButton = new ToggleButton(TRIGGER_HIGH_DISABLED_BUTTON_ICONS, TRIGGER_HIGH_ENABLED_BUTTON_ICONS);
        ToggleButton fallingButton = new ToggleButton(TRIGGER_FALLING_DISABLED_BUTTON_ICONS, TRIGGER_FALLING_ENABLED_BUTTON_ICONS);
        ToggleButton lowButton = new ToggleButton(TRIGGER_LOW_DISABLED_BUTTON_ICONS, TRIGGER_LOW_ENABLED_BUTTON_ICONS);

        risingButton.addActionListener(actionEvent -> this.setTrigger(ChannelConfiguration.Trigger.RISING));
        highButton.addActionListener(actionEvent -> this.setTrigger(ChannelConfiguration.Trigger.HIGH));
        fallingButton.addActionListener(actionEvent -> this.setTrigger(ChannelConfiguration.Trigger.FALLING));
        lowButton.addActionListener(actionEvent -> this.setTrigger(ChannelConfiguration.Trigger.LOW));

        this.triggerButtons = new LinkedButtonPanel(new ToggleButton[]{risingButton, highButton, fallingButton, lowButton}, LinkedButtonPanel.X_AXIS, SMALL_COMPONENT_GAP);

        triggerPanel.add(Box.createRigidArea(new Dimension(SMALL_COMPONENT_GAP, SMALL_COMPONENT_GAP)));
        triggerPanel.add(Styler.style(new JLabel("Triggers: "), Styler.Style.CHANNEL_CONTROL_PANEL));

        triggerPanel.add(Box.createHorizontalGlue());
        triggerPanel.add(new JSeparator(SwingConstants.VERTICAL));

        triggerPanel.add(this.triggerButtons);
        triggerPanel.add(Box.createRigidArea(new Dimension(MEDIUM_COMPONENT_GAP, 0)));

        triggerPanel.add(new JSeparator(SwingConstants.VERTICAL));
        triggerPanel.add(Box.createHorizontalGlue());

        controlPanel.add(createControlPanelHeader(channelName));
        controlPanel.add(triggerPanel);

        Styler.setSize(triggerPanel, CHANNEL_CONTROL_PANEL_WIDTH, triggerPanel.getPreferredSize().height);

        return controlPanel;
    }

    /**
     * Creates the display panel for this channel.
     *
     * @return The display panel as a {@link JPanel}.
     */
    private JPanel createDisplayPanel() {

        JPanel displayPanel = new JPanel(new BorderLayout());
        displayPanel.setBorder(BorderFactory.createEtchedBorder());

        this.plotPanel = new PlotPanel(true);
        plotPanel.setAlignmentX(Component.LEFT_ALIGNMENT);
        plotPanel.setAxisType(PlotPanel.Axis.VERTICAL, PlotPanel.AxisType.FIXED_LABEL);
        plotPanel.setAxisType(PlotPanel.Axis.HORIZONTAL, PlotPanel.AxisType.DYNAMIC_LABEL);

        displayPanel.add(createDisplayPanelHeader(), BorderLayout.PAGE_START);
        displayPanel.add(plotPanel, BorderLayout.CENTER);

        return displayPanel;
    }

    /**
     * Sets the trigger for this channel.
     *
     * @param trigger The trigger value as an integer.
     */
    private void setTrigger(ChannelConfiguration.Trigger trigger) {

        synchronized (this) {
            if (this.trigger == trigger) this.trigger = ChannelConfiguration.Trigger.NONE;
            else this.trigger = trigger;
        }

        synchronized (this.triggerSelectionListeners) {
            this.triggerSelectionListeners.forEach(listener -> listener.triggerSelected(trigger));
        }
    }

    /**
     * Sets the channels visibility state. If false only the header panel of the channel will be visible.
     *
     * @param isCollapsed The new visibility state for the channel.
     */
    private void setCollapsed(boolean isCollapsed) {

        this.isCollapsed = isCollapsed;

        int channelHeight = isCollapsed ? CHANNEL_HEADER_HEIGHT : OPEN_CHANNEL_HEIGHT;

        Styler.setSize(this.control, CHANNEL_CONTROL_PANEL_WIDTH, channelHeight);
        Styler.setSize(this.display, this.displayPanelWidth, channelHeight);
    }

    /**
     * Updates the sampling progress of the current channel to the specified amount.
     *
     * @param currentSampleCount The number of samples currently displayed on this channel's plot.
     */
    private void updateSamplingProgress(int currentSampleCount) {

        if(this.samplingProgress.isIndeterminate()) this.samplingProgress.setIndeterminate(false);
        if(!this.samplingProgress.isStringPainted()) this.samplingProgress.setStringPainted(true);

        this.samplingProgress.setValue(currentSampleCount);

        if(currentSampleCount == this.samplingProgress.getMaximum()) {

            logger.info("Sampling Complete on channel " + this.getChannelIndex());
            this.samplingProgress.setString("Sampling Complete");
        } else {
            this.samplingProgress.setString("Collected: " + currentSampleCount + "/" + this.samplingProgress.getMaximum());
        }
    }

    /**
     * Returns a formatted version of the specified channel name meant to be passed to a {@link JLabel} object.
     *
     * @param channelName The channel name to be formatted.
     * @return The formatted channel name as a {@link String}.
     */
    private String wrapChannelName(String channelName) {
        return "<html>" + channelName + " (" + this.getChannelIndex() + ")</html>";
    }

    /** === Static Methods ------------------------------------------------ **/

    /**
     * Returns the channel corresponding to the specified index. A valid channel must exist for the specified index.
     *
     * @param channel The index of the channel.
     * @return The channel as a Channel object.
     */
    public static Channel getChannel(int channel) {

        assert usedChannels.containsKey(channel): "A channel has not been created for index " + channel;

        return getChannel(channel, null);
    }

    /**
     * Returns the channel corresponding to the specified index. The Alias of the channel
     * can be specified via {@code channelName}. If a channel for the specified index already
     * exists, the name will be overridden.
     *
     * @param channel The index of the channel.
     * @param channelName The display name for the channel.
     * @return The channel as a Channel object.
     */
    public static Channel getChannel(int channel, String channelName) {

        assert (1 <= channel && channel <= N_CHANNELS):(channel + " is not a valid channel index.");

        if(!usedChannels.containsKey(channel)) {

            assert channelName != null : "The name of a new channel cannot be null.";

            usedChannels.put(channel, new Channel(channel, channelName));
            unusedChannelIndices.remove(channel);
        }
        else if(channelName != null) usedChannels.get(channel).setChannelName(channelName);

        return usedChannels.get(channel);
    }

    /**
     * Frees the channel index corresponding to the given channel index and removes the reference
     * to this {@link Channel} object.
     *
     * @param channel The channel to be released as a {@link Channel} object.
     */
    public static void releaseChannel(Channel channel) {

        assert channel != null : "The specified channel cannot be null.";
        assert usedChannels.containsKey(channel.getChannelIndex()) : "The specified channel doesn't exist.";

        usedChannels.remove(channel.getChannelIndex());
        unusedChannelIndices.add(channel.getChannelIndex());
    }

    /**
     * Returns an unmodifiable set of all the channel indices that have not yet been used.
     *
     * @return The set of indices of all unused channels.
     */
    public static Set<Integer> getUnusedChannelIndices() {return Collections.unmodifiableSet(unusedChannelIndices);}


    /** === Instance Variables ----------------------------------------------- **/

    private final JPanel control;
    private final JPanel display;

    private Button removeButton;
    private Button editButton;
    private Button reorderUpButton;
    private Button reorderDownButton;
    private LinkedButtonPanel triggerButtons;
    private JLabel channelNameLabel;
    private JCheckBox channelSelected;
    private PlotPanel plotPanel;

    private JProgressBar samplingProgress;

    private String channelName;
    private final int channelIndex;

    private boolean isCollapsed;
    private boolean isSelected;
    private boolean isFirst;
    private boolean isLast;
    private boolean isCleared;

    private ChannelConfiguration.Trigger trigger;
    private final List<TriggerSelectionListener> triggerSelectionListeners;
    private final List<ChannelReorderingListener> channelReorderingListeners;

    private int displayPanelWidth;


    /** === Static Variables ------------------------------------------------- **/

    private static final Map<Integer, Channel> usedChannels;
    private static final Set<Integer> unusedChannelIndices;

    static {
        usedChannels = new HashMap<>();
        unusedChannelIndices = IntStream.rangeClosed(1, N_CHANNELS).boxed().collect(Collectors.toSet());
    }
}
