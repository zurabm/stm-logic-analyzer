package channel;

import data.DataValidator;
import styling.Styler;
import textfield.TextField;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import static styling.GUIConstants.*;

public class ChannelOptionsFrame extends JFrame {

    /**
     * Represents a Record class for storing the user specified information when
     * creating a new channel.
     */
    public static record ChannelInfo(int channelIndex, String channelName) {}


    /** === Interface Functions ---------------------------------------------- **/

    /**
     * Opens a Channel Options Frame for adding a new channel to a panel. If one is
     * already opened it will be moved to the center of the screen.
     *
     * @param channelNameValidator A {@link DataValidator} to be used when checking the specified name of the channel.
     *
     * @return channelInfo The specified channel information of the new channel as a {@link ChannelInfo} object.
     */
    public static ChannelInfo open(DataValidator channelNameValidator) {

        openLock.lock();
        if (openedFrame == null) {

            openedFrame = new ChannelOptionsFrame(channelNameValidator);
            openLock.unlock();

            ChannelInfo channelInfo = openedFrame.getChannelInfo();

            openLock.lock();
            openedFrame.dispose();
            openedFrame = null;
            openLock.unlock();

            return channelInfo;
        } else {

            Styler.centerPopUp(openedFrame);;
            openedFrame.toFront();

            if(openedFrame.getExtendedState() == ICONIFIED) {
                openedFrame.setExtendedState(NORMAL);
            }

            openLock.unlock();

            return null;
        }
    }

    /**
     * Adds a component to the main settings panel.
     *
     * @param component The component to be added as a {@link Component} object.
     * @return The component that was added.
     */
    @Override
    public Component add(Component component) {return this.mainPanel.add(component);}


    /** === Private Methods ----------------------------------------------- **/

    /**
     * Constructor for the ChannelOptionsFrame object. This components
     * is used by the GUI whenever a new Channel is being added by the user as
     * a way to specify the channel parameters.
     *
     * @param channelNameValidator A {@link DataValidator} to be used when checking the specified name of the channel.
     */
    private ChannelOptionsFrame(DataValidator channelNameValidator) {

        super("Add New Channel");

        this.mainPanel = new JPanel();
        this.responseLatch = new CountDownLatch(1);

        this.setLayout();
        this.setMargins();

        this.add(createChannelInfoPanel(channelNameValidator));
        this.add(Box.createRigidArea(new Dimension(GENERIC_COMPONENT_GAP, GENERIC_COMPONENT_GAP)));
        this.add(createButtonPanel());

        setResizable(false);

        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        this.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosed(WindowEvent e) {ChannelOptionsFrame.this.setNull();}
        });

        pack();
        setVisible(true);

        Styler.centerPopUp(this);
    }

    /**
     * Sets the layout for this channel options frame.
     */
    private void setLayout() {

        super.setLayout(new BorderLayout());
        super.add(this.mainPanel, BorderLayout.CENTER);

        this.mainPanel.setLayout(new BoxLayout(this.mainPanel, BoxLayout.Y_AXIS));
    }

    /**
     * Sets the margins at the edges of this channel options frame.
     */
    private void setMargins() {
        this.mainPanel.setBorder(new EmptyBorder(CHANNEL_OPTIONS_FRAME_MARGINS));
    }

    /**
     * Returns a JPanel object with a JTextField and a JComboBox
     * for specifying all the relevant information of a new Channel.
     *
     * @param channelNameValidator A {@link DataValidator} to be used when checking the specified name of the channel.
     *
     * @return Channel Information Panel as a JPanel
     */
    private JPanel createChannelInfoPanel(DataValidator channelNameValidator) {

        JPanel channelInfoPanel = new JPanel();
        channelInfoPanel.setLayout(new BoxLayout(channelInfoPanel, BoxLayout.Y_AXIS));

        // Add Channel Name TextField

        JPanel channelNamePanel = new JPanel();
        channelNamePanel.setLayout(new BoxLayout(channelNamePanel, BoxLayout.X_AXIS));

        this.channelNameField = new TextField(GENERIC_TEXT_FIELD_COLUMNS, channelNameValidator);

        channelNamePanel.add(Styler.style(new JLabel("Channel Name:"), Styler.Style.POP_UP));
        channelNamePanel.add(Box.createHorizontalGlue());
        channelNamePanel.add(Box.createRigidArea(new Dimension(MEDIUM_COMPONENT_GAP, MEDIUM_COMPONENT_GAP)));
        channelNamePanel.add(Styler.style(this.channelNameField, Styler.Style.POP_UP));

        // Add Channel Select JComboBox

        JPanel channelSelectPanel = new JPanel();
        channelSelectPanel.setLayout(new BoxLayout(channelSelectPanel, BoxLayout.X_AXIS));

        this.channelSelectBox = new JComboBox(Channel.getUnusedChannelIndices().toArray());
        this.channelSelectBox.setPreferredSize(this.channelNameField.getPreferredSize());

        channelSelectPanel.add(Styler.style(new JLabel("Channel Index:"), Styler.Style.POP_UP));
        channelSelectPanel.add(Box.createHorizontalGlue());
        channelSelectPanel.add(Box.createRigidArea(new Dimension(MEDIUM_COMPONENT_GAP, MEDIUM_COMPONENT_GAP)));
        channelSelectPanel.add(Styler.style(this.channelSelectBox, Styler.Style.POP_UP));

        channelInfoPanel.add(channelNamePanel);
        channelInfoPanel.add(channelSelectPanel);

        KeyAdapter enterKeyListener = new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                if(e.getKeyCode() == KeyEvent.VK_ENTER) ChannelOptionsFrame.this.setChannelInfo();
                else if(e.getKeyCode() == KeyEvent.VK_ESCAPE) ChannelOptionsFrame.this.setNull();
            }
        };

        channelNameField.addKeyListener(enterKeyListener);
        channelSelectBox.addKeyListener(enterKeyListener);

        return channelInfoPanel;
    }

    /**
     * @return A {@link JPanel} object will all the necessary buttons for a channel options frame.
     */
    private JPanel createButtonPanel() {

        JPanel buttonPanel = new JPanel();
        buttonPanel.setLayout(new BoxLayout(buttonPanel, BoxLayout.X_AXIS));

        JButton addButton = new JButton("Add");
        JButton cancelButton = new JButton("Cancel");

        addButton.addActionListener(actionEvent -> this.setChannelInfo());
        cancelButton.addActionListener(actionEvent -> this.setNull());

        buttonPanel.add(Box.createHorizontalGlue());
        buttonPanel.add(Styler.style(addButton, Styler.Style.POP_UP));
        buttonPanel.add(Box.createHorizontalGlue());
        buttonPanel.add(Styler.style(cancelButton, Styler.Style.POP_UP));
        buttonPanel.add(Box.createHorizontalGlue());

        return buttonPanel;
    }

    /**
     * Collects the user specified data and returns it once the user presses the apply button.
     *
     * @return The user specified data as a {@link ChannelInfo} object.
     */
    private ChannelInfo getChannelInfo() {

        try {
            this.responseLatch.await();
        } catch (InterruptedException ignored) {}

        return this.channelInfo;
    }

    /**
     * Validates the user specified data and notifies another thread to return the specified information to the
     * thread that created the {@link ChannelOptionsFrame}.
     */
    private void setChannelInfo() {

        if(this.channelNameField.validateData()) {

            assert this.channelSelectBox.getSelectedItem() != null: "An available channel must be selected.";

            int channelIndex = (Integer) this.channelSelectBox.getSelectedItem();
            String channelName = this.channelNameField.getText().trim();

            this.channelInfo = new ChannelInfo(channelIndex, channelName);
            this.responseLatch.countDown();
        }
    }

    /**
     * Notifies the thread waiting for the user response and sets a null value for the user specified information. This
     * is used to signal a cancel or window closed operation from the user.
     */
    private void setNull() {

        this.channelInfo = null;
        this.responseLatch.countDown();
    }

    /** === Private Instance Variables ------------------------------------ **/

    private final JPanel mainPanel;
    private TextField channelNameField;
    private JComboBox<Integer> channelSelectBox;

    private ChannelInfo channelInfo;
    private final CountDownLatch responseLatch;

    /** === Private Class Variables --------------------------------------- **/

    private static ChannelOptionsFrame openedFrame;
    private static final Lock openLock;

    static {
        openLock = new ReentrantLock();
    }
}
