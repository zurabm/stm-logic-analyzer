package boxes;

import styling.Styler;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import static styling.GUIConstants.*;

public class ConfirmationBox extends LinkedPopupBox {

    /** === Interface Functions ---------------------------------------------- **/

    /**
     * Prompts the user and waits for their confirmation/rejection. The current thread will be blocked until
     * the user responds or closes the window.
     *
     * @param prompt The message to be displayed to the user.
     * @param link The object this confirmation box is linked to. If this method is called with the same link before the
     *             previous window is closed, a new one will not be created and the previous box will be brought to the
     *             front of the screen.
     *
     * @return The user's response or false if this method is called a second time before the previous window is closed.
     */
    public static boolean confirm(String prompt, Object link) {

        Object response = open(link, ConfirmationBox.class, prompt);
        if(response == NO_RESPONSE) {
            return false;
        } else {
            return (boolean) response;
        }
    }


    /** === Private Methods -------------------------------------------------- **/

    /**
     * Creates a confirmation box with the specified prompt.
     *
     * @param prompt The message to be displayed to the user.
     */
     protected ConfirmationBox(String prompt) {

        super("Confirm", BoxLayout.Y_AXIS, CONFIRMATION_BOX_MARGINS);

        this.addPrompt(prompt);
        this.addControlButtons();

        this.setResizable(false);

        this.setMinimumSize(new Dimension(CONFIRMATION_POPUP_WIDTH, CONFIRMATION_POPUP_HEIGHT));
        Styler.centerPopUp(this);

        this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        this.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosed(WindowEvent e) {
                ConfirmationBox.this.setResponse(false);
            }
        });

        this.pack();
        this.setVisible(true);
     }

    /**
     * Adds the specified text to the center of this {@link ConfirmationBox}.
     *
     * @param prompt The message to be displayed to the user.
     */
    private void addPrompt(String prompt) {

        JPanel promptPanel = new JPanel();
        promptPanel.setLayout(new BorderLayout());
        promptPanel.setAlignmentX(CENTER_ALIGNMENT);
        promptPanel.setAlignmentY(CENTER_ALIGNMENT);

        prompt = prompt.replace("\n", "<br>");

        promptPanel.add(
                Styler.style(new JLabel("""
                        <html>
                            <p style='text-align:center;'>
                                %s
                            </p>
                        </html>
                        """.formatted(prompt)), Styler.Style.POP_UP),
                BorderLayout.CENTER
        );

        this.add(promptPanel);
    }

    /**
     * Adds control buttons for the user to this {@link NotificationBox}.
     */
    private void addControlButtons() {

        JPanel controlButtonPanel = new JPanel();
        controlButtonPanel.setLayout(new BoxLayout(controlButtonPanel, BoxLayout.X_AXIS));

        JButton yesButton = new JButton("Yes");
        JButton noButton = new JButton("No");

        yesButton.addActionListener(actionEvent -> this.setResponse(true));
        noButton.addActionListener(actionEvent -> this.setResponse(false));

        controlButtonPanel.add(Box.createHorizontalGlue());
        controlButtonPanel.add(yesButton);
        controlButtonPanel.add(Box.createHorizontalGlue());
        controlButtonPanel.add(noButton);
        controlButtonPanel.add(Box.createHorizontalGlue());

        this.add(controlButtonPanel);
    }
}
