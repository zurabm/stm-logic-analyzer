package boxes;

import popups.PopupTextPanel;
import styling.Styler;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;
import java.util.*;

import static styling.GUIConstants.GENERIC_COMPONENT_GAP;
import static styling.GUIConstants.SMALL_COMPONENT_GAP;

public class FieldBox extends Box {

    public static class Field extends Box {

        private static Field from(String fieldName, Object[] possibleValues) {
            return new Field(fieldName, possibleValues);
        }

        private Field(String fieldName, Object[] possibleValues) {

            super(BoxLayout.X_AXIS);

            this.fieldName = fieldName;
            this.comboBox = new JComboBox<>(possibleValues);

            this.addComponents();
        }

        public String getName() {
            return this.fieldName;
        }

        public Object getValue() {
            return this.comboBox.getSelectedItem();
        }

        public void setValue(Object value){
            this.comboBox.setSelectedItem(value);
        }

        public void setPossibleValues(Object[] possible) {

            this.comboBox.removeAll();
            Arrays.stream(possible).forEachOrdered(this.comboBox::addItem);
        }

        public void addActionListener(ActionListener actionListener) {
            this.comboBox.addActionListener(actionListener);
        }

        private void addComponents() {

            this.add(Box.createRigidArea(new Dimension(SMALL_COMPONENT_GAP, GENERIC_COMPONENT_GAP)));
            this.add(Styler.style(new JLabel(this.fieldName)));
            this.add(Box.createHorizontalGlue());
            this.add(Styler.style(this.comboBox, Styler.Style.LONG));
            this.add(Box.createRigidArea(new Dimension(SMALL_COMPONENT_GAP, GENERIC_COMPONENT_GAP)));

            MouseAdapter closePopupAdapter = new MouseAdapter() {
                @Override
                public void mousePressed(MouseEvent e) {
                    PopupTextPanel.removePopup(Field.this.comboBox);
                }
            };

            List.of(this, this.comboBox).forEach(component -> component.addMouseListener(closePopupAdapter));
        }

        private final String fieldName;
        private final JComboBox<Object> comboBox;
    }

    private final Map<String, Field> fields;
    private final Map<String, FieldBox> fieldBoxes;

    private static final Comparator<String> fieldComparator = (string1, string2) -> {
        int compareResult = Integer.compare(string1.length(), string2.length());
        if (compareResult != 0) {
            return compareResult;
        } else {
            return String.CASE_INSENSITIVE_ORDER.compare(string1, string2);
        }
    };

    public static FieldBox createEmptyBox(String boxName) {
        return (FieldBox) Styler.style(new FieldBox(boxName));
    }

    public Map<String, Object> getValues() {

        Map<String, Object> values = new HashMap<>();

        for(String field: this.fields.keySet()) {
            values.put(field, this.fields.get(field).getValue());
        }

        for(String block: this.fieldBoxes.keySet()) {
            values.putAll(this.fieldBoxes.get(block).getValues());
        }

        return values;
    }

    public List<String> getFields() {return this.fields.keySet().stream().toList();}

    public List<String> getAllFields(){

        List<String> fieldNames = new ArrayList<>(this.fields.keySet().stream().toList());

        for(FieldBox fieldBox: this.fieldBoxes.values()){
            fieldNames.addAll(fieldBox.getAllFields());
        }

        return fieldNames;
    }

    public FieldBox clearAllFields(){

        this.fields.clear();
        this.fieldBoxes.clear();
        this.removeAll();

        this.revalidate();
        this.repaint();

        this.checkFieldCount();

        return this;
    }

    public FieldBox addField(String fieldName, Object[] possibleValues) {

        Objects.requireNonNull(fieldName, "The field name must be specified.");
        Objects.requireNonNull(possibleValues, "The possible values must be specified.");

        assert !this.fields.containsKey(fieldName): "A field with the name " + fieldName + " already exists.";

        Field field = Field.from(fieldName, possibleValues);

        if(!this.fields.isEmpty()) {
            Styler.addSeparator(this, JSeparator.HORIZONTAL, false);
        }

        this.fields.put(fieldName, field);
        this.add(field);

        this.checkFieldCount();

        return this;
    }

    public Object getSelectedValue(String fieldName){

        Objects.requireNonNull(fieldName, "The field name must be specified.");

        Field field = this.getField(fieldName);

        assert field != null: "A field with the name " + fieldName + " doesn't exist.";

        return field.getValue();
    }

    public void setSelectedValue(String fieldName, Object value){

        Objects.requireNonNull(fieldName, "The field name must be specified.");

        Field field = this.getField(fieldName);

        assert field != null: "A field with the name " + fieldName + " doesn't exist.";

        field.setValue(value);
    }

    public void setPossibleValues(String fieldName, Object[] possibleValues){

        Objects.requireNonNull(fieldName, "The field name must be specified.");
        Objects.requireNonNull(possibleValues, "The possible values must be specified.");

        Field field = this.getField(fieldName);

        assert field != null: "A field with the name " + fieldName + " doesn't exist.";

        field.setPossibleValues(possibleValues);
    }

    public void addActionListener(String fieldName, ActionListener actionListener) {

        Field field = this.getField(fieldName);

        assert field != null: "The field " + fieldName + " doesn't exist.";

        field.addActionListener(actionListener);
    }

    public FieldBox addText(String text) {

        Objects.requireNonNull(text, "The specified text cannot be null.");

        Box textBox = Box.createHorizontalBox();
        textBox.add(Styler.style(new JLabel(text)));
        textBox.add(Box.createHorizontalGlue());

        this.add(textBox);

        this.checkFieldCount();

        return this;
    }

    public void displayError(String fieldName, String error) {

        Field field = this.getField(fieldName);

        assert field != null: "The field " + fieldName + " doesn't exist.";

        PopupTextPanel.displayPopup(field.comboBox, "This channel is already selected.",
                PopupTextPanel.PopupType.ERROR, PopupTextPanel.PopupLocation.RIGHT, true);
    }

    public FieldBox addBox(String boxName) {

        Objects.requireNonNull(boxName, "The box name must be specified.");

        assert !this.fieldBoxes.containsKey(boxName): "A box with the name " + boxName + " already exists.";

        FieldBox fieldBox = FieldBox.createEmptyBox(boxName);
        this.fieldBoxes.put(boxName, fieldBox);
        this.add(fieldBox);

        this.checkFieldCount();

        return fieldBox;
    }

    public FieldBox getBox(String boxName) {

        Objects.requireNonNull(boxName, "The box name must be specified.");

        assert this.fieldBoxes.containsKey(boxName): "A box with the name " + boxName + " doesn't exists.";

        return this.fieldBoxes.get(boxName);
    }


    private FieldBox(String name){

        super(BoxLayout.Y_AXIS);

        this.fields = new TreeMap<>(fieldComparator);
        this.fieldBoxes = new TreeMap<>(fieldComparator);
        this.boxName = name;

        this.checkFieldCount();
    }

    private void checkFieldCount() {

        if(this.boxName == null || (this.fields.isEmpty() && this.fieldBoxes.isEmpty())) {
            this.setBorder(null);
        } else {
            this.setBorder(BorderFactory.createTitledBorder(this.boxName));
        }
    }

    private Field getField(String fieldName) {

        Objects.requireNonNull(fieldName, "The field name can not be null.");

        if(this.fields.containsKey(fieldName)) {
            return this.fields.get(fieldName);
        }

        for(FieldBox fieldBox: this.fieldBoxes.values()) {

            Field field = fieldBox.getField(fieldName);
            if(field != null) {
                return field;
            }
        }

        return null;
    }

    public static void main (String [] args){

        JFrame testFrame = new JFrame();

        FieldBox testBox = FieldBox.createEmptyBox("TestBox")
                .addField("Test field1", new Object[]{"1", "2", "3", "4", 5, 6})
                .addField("Test field2", new Object[]{"1", "2", "3", "4", 5, 6, 7})
                .addField("Test field3", new Object[]{"1", "2", "3", "4", 5, 6, 7, 8});
        testBox.addBox("Test Box box")
                .addField("Test field4", new Object[]{"1", "2", "3", "4"})
                .addField("Test field5", new Object[]{"1", "2", "3", "4", 5})
                .addField("Test field6", new Object[]{"1", "2", "3", "4", 5, 6});

        testFrame.add(testBox);
        testFrame.pack();
        testFrame.setVisible(true);
        testFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

    }

    private final String boxName;
}
