package boxes;

import styling.Styler;

import javax.swing.*;
import javax.swing.text.html.HTMLEditorKit;
import javax.swing.text.html.StyleSheet;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import static styling.GUIConstants.*;

public class NotificationBox extends PopupBox {

    private static final int MAX_OPEN_BOXES = 10;


    /** === Enum Definitions ------------------------------------------------- **/

    public enum Alignment {
        LEFT("<div style='text-align:left;'>%s</div>"),
        RIGHT("<div style='text-align:right;'>%s</div>"),
        CENTER("<div style='text-align:center;'>%s</div>");

        Alignment(String format) {this.format = format;}

        private final String format;
    }

    /** === Interface Functions ---------------------------------------------- **/

    /**
     * Creates and displays a notification box showing the specified message.
     *
     * @param message The message as a {@link String}.
     * @param alignment The alignment of the displayed text.
     */
    public static void display(String message, Alignment alignment) {

        try {
            openLock.lock();
            openBoxes.add(new NotificationBox(message, alignment));
        } finally {
            openLock.unlock();
        }

        if(openBoxes.size() > MAX_OPEN_BOXES) {
            openBoxes.stream().findFirst().orElseThrow().dispose();
        }
    }

    /**
     * Creates and displays a notification box showing the specified message.
     *
     * @param message The message as a {@link String}.
     */
    public static void display(String message) {
        display(message, Alignment.CENTER);
    }

    /**
     * Validates a condition and displays the specified error message if the condition is false.
     *
     * @param condition The condition to be checked.
     * @param errorMessage The error message as a {@link String}.
     * @return True if the condition is true, false otherwise.
     */
    public static boolean check(boolean condition, String errorMessage) {

        if(!condition) display(errorMessage);

        return condition;
    }


    /** === Private Methods -------------------------------------------------- **/

    /**
     * Creates a notification box specifying the given message.
     *
     * @param message The message to be displayed as a {@link String}.
     * @param alignment The alignment of the displayed text.
     */
    private NotificationBox(String message, Alignment alignment) {

        super("Notification", BoxLayout.Y_AXIS, NOTIFICATION_BOX_MARGINS);

        this.addMessage(message, alignment);
        this.addControlButtons();

        this.setMinimumSize(new Dimension(ERROR_PANEL_WIDTH, ERROR_PANEL_HEIGHT));
        this.setResizable(false);

        Styler.centerPopUp(this);

        this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);

        this.pack();
        this.setVisible(true);

        this.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosed(WindowEvent e) {
                try {
                    openLock.lock();
                    openBoxes.remove(NotificationBox.this);
                } finally {
                    openLock.unlock();
                }
            }
        });
    }

    /**
     * Displays the specified text in the center of this pop-up.
     *
     * @param message The text to be displayed as a {@link String}.
     * @param alignment The alignment of the displayed text.
     */
    private void addMessage(String message, Alignment alignment) {

        JPanel errorMessagePanel = new JPanel();
        errorMessagePanel.setLayout(new BorderLayout());
        errorMessagePanel.setAlignmentX(CENTER_ALIGNMENT);
        errorMessagePanel.setAlignmentY(CENTER_ALIGNMENT);

        message = message.replace("\n", "<br>");

        HTMLEditorKit editorKit = new HTMLEditorKit();
        StyleSheet styleSheet = editorKit.getStyleSheet();
        styleSheet.addRule(""" 
                table {
                width: 100%;
                }
                
                th, td {
                    padding: 0px;
                    spacing: 0px;
                    border-bottom: 1px solid #DDD;
                }
                """);

        JEditorPane messagePane = new JEditorPane();
        messagePane.setEditorKit(editorKit);
        messagePane.setContentType("text/html");
        messagePane.setEditable(false);
        messagePane.setText("<html>%s</html>".formatted(alignment.format).formatted(message));

        messagePane.setAlignmentX(CENTER_ALIGNMENT);

        errorMessagePanel.add(Styler.style(messagePane, Styler.Style.POP_UP), BorderLayout.CENTER);
        this.add(new JScrollPane(errorMessagePanel,
                JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED));
    }

    /**
     * Adds control buttons for the user to this {@link NotificationBox}.
     */
    private void addControlButtons() {

        JPanel buttonPanel = new JPanel();
        buttonPanel.setLayout(new BoxLayout(buttonPanel, BoxLayout.Y_AXIS));

        JButton closeButton = new JButton("Ok");
        closeButton.setAlignmentX(CENTER_ALIGNMENT);

        closeButton.addActionListener(actionEvent -> this.dispose());

        buttonPanel.add(closeButton);

        this.add(Box.createVerticalGlue());
        this.add(buttonPanel);
    }


    /** === Static Variables ------------------------------------------------- **/

    private static final Set<NotificationBox> openBoxes;
    private static final Lock openLock;

    static {
        openBoxes = new LinkedHashSet<>();
        openLock = new ReentrantLock();
    }
}
