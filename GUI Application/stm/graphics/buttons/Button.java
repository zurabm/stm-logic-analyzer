package buttons;

import javax.swing.*;
import java.util.Objects;

/**
 * Class: Button
 * Date Created: 08.07.21 21:44
 *
 * === Description ============================================= *
 * This class is just like a regular {@link JButton}, the only difference
 * being that the images for the various states of the button can
 * be specified.
 */
public class Button extends JButton {

    /** === Interface Functions ---------------------------------------------- **/

    /**
     * Creates a Button from the specified icons.
     *
     * @param iconRecord The icons as a {@link ButtonIconRecord}.
     */
    public Button(ButtonIconRecord iconRecord) {

        Objects.requireNonNull(iconRecord, "The icon record for a Button cannot be null.");

        this.setIcon(iconRecord.defaultIcon());
        this.setRolloverIcon(iconRecord.rolloverIcon());
        this.setPressedIcon(iconRecord.pressedIcon());
        this.setDisabledIcon(iconRecord.disabledIcon());
    }

    /**
     * Sets a new set of icons for the button.
     *
     * @param newIconRecord The icons as a {@link ButtonIconRecord}.
     */
    public void setIcons(ButtonIconRecord newIconRecord) {

        this.setIcon(newIconRecord.defaultIcon());
        this.setRolloverIcon(newIconRecord.rolloverIcon());
        this.setPressedIcon(newIconRecord.pressedIcon());
        this.setDisabledIcon(newIconRecord.disabledIcon());
    }
}
