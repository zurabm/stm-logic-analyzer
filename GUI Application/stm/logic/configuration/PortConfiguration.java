package configuration;

import com.fazecast.jSerialComm.SerialPort;

public record PortConfiguration(String selectedPortName) {

    /**
     * Creates a port configuration record with the specified parameters.
     *
     * @param selectedPortName The system name of the port provided by {@link SerialPort} using {@code getSystemName()}.
     */
    public PortConfiguration {}
}
