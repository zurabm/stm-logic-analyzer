package protocols.protocol;

public enum ProtocolType {

    PARALLEL_ENCODER("Parallel Encoder"),
    I2C("I2C"),
    SPI("SPI"),
    UART("UART");


    /** === Interface Functions ---------------------------------------------- **/

    /**
     * @return A human-readable string representation of this protocol's name.
     */
    @Override
    public String toString() {return this.value;}


    /** === Private Functions ------------------------------------------------ **/

    /**
     * Creates a protocol type with the specified string as it's readable value.
     *
     * @param value The string representation of the protocol.
     */
    ProtocolType(String value) {this.value = value;}


    /** === Instance Variables ----------------------------------------------- **/

    private final String value;
}