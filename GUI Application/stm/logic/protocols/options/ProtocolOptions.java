package protocols.options;

import logging.ConfiguredLogger;
import protocols.protocol.Protocol;
import protocols.protocol.ProtocolType;

import java.util.Collections;
import java.util.Map;
import java.util.logging.Logger;

public abstract class ProtocolOptions {

    private static final Logger logger = ConfiguredLogger.getLogger("ProtocolOptions");

    /** === Interface Functions ---------------------------------------------- **/

    /**
     * Creates a new {@link ProtocolOptions} of the given type from the specified options mapping.
     *
     * @param type The protocol type of the new {@link Protocol} as a {@link ProtocolType}.
     * @param protocolOptions Options for the protocol of the specified type as a {@link Map}.
     *
     * @return A new {@link ProtocolOptions} object of the specified type.
     */
    public static ProtocolOptions from(ProtocolType type, Map<String, Object> protocolOptions) {

        ProtocolOptions options;

        switch (type) {
            case PARALLEL_ENCODER -> options = new ParallelEncoderOptions(protocolOptions);
            case I2C -> options = new I2COptions(protocolOptions);
            case SPI -> options = new SPIOptions(protocolOptions);
            case UART -> options = null;
            default -> throw new RuntimeException(type + " is not a valid protocol type.");
        }

        return options;
    }


    /** === Private Functions ------------------------------------------------ **/

    /**
     * Creates a {@link ProtocolOptions} object from the specified map.
     *
     * @param options The protocol options as a {@link Map}.
     */
    protected ProtocolOptions(Map<String, Object> options) {
        this.options = Collections.unmodifiableMap(options);
    }

    /**
     * Returns the value associated with the specified option.
     *
     * @param optionName The name of the option field.
     *
     * @return The value of the option field as an {@link Object}.
     */
    protected final Object getOption(String optionName) {

        assert this.options.containsKey(optionName): "The option " + optionName + " is not specified.";
        return this.options.get(optionName);
    }

    /**
     * Returns the value for the specified option as a {@link String}.
     *
     * @param optionName The name of the option field.
     *
     * @return The value of the option field as a {@link String}.
     */
    protected final String getStringOption(String optionName) {

        assert this.options.containsKey(optionName): "The option " + optionName + " is not specified.";
        return this.options.get(optionName).toString();
    }

    /**
     * Returns the value for the specified option as an integer.
     *
     * @param optionName The name of the option field.
     *
     * @return The value of the option field as an integer.
     */
    protected final int getIntOption(String optionName) {

        try {
            return Integer.parseInt(this.getStringOption(optionName));
        } catch (NumberFormatException e) {

            logger.severe("Couldn't parse an integer value for the option " + optionName);
            e.printStackTrace();

            return 0;
        }
    }


    /** === Instance Variables ----------------------------------------------- **/

    private final Map<String, Object> options;
}
