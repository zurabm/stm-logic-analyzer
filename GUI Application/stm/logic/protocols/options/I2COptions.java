package protocols.options;

import java.util.Map;

public final class I2COptions extends ProtocolOptions {

    private static final String N_ADDRESS_BITS = "Number of address bits";

    private static final Integer[] ADDRESS_BIT_OPTION_VALUES = new Integer[] {7,10};


    /** === Interface Functions ---------------------------------------------- **/

    /**
     * @return The name of the field specifying the number of addressing bits used in this protocol.
     */
    public static String getAddressBitCountOption() {return N_ADDRESS_BITS;}

    /**
     * @return The possible address bit options for an I2C protocol.
     */
    public static Integer[] getAddressBitOptionValues() {return ADDRESS_BIT_OPTION_VALUES;}

    /**
     * @return The number of addressing bits specified for this {@link I2COptions}.
     */
    public int getAddressBitCount() {return this.getIntOption(getAddressBitCountOption());}


    /** === Private Functions ------------------------------------------------ **/

    /**
     * Creates a {@link ProtocolOptions} object from the specified map.
     *
     * @param options The protocol options as a {@link Map}.
     */
    protected I2COptions(Map<String, Object> options) {super(options);}
}
