package decoder;

import decoder.data.frame.ParallelEncoderFrame;
import decoder.data.message.DecodedMessage;
import decoder.data.message.ParallelEncoderMessage;
import protocols.protocol.ParallelEncoder;
import utility.Timestamp;

import java.util.*;
import java.util.stream.IntStream;

public class ParallelEncoderDecoder extends Decoder {

    /** === Interface Functions ---------------------------------------------- **/

    @Override
    public List<Integer> getUsedChannelIndices() {

        ParallelEncoder protocol = this.getProtocol();
        List<Integer> channelIndices = new ArrayList<>();

        channelIndices.add(protocol.getClockChannel());
        channelIndices.addAll(IntStream.range(0, protocol.getProtocolOptions().getDataChannelCount())
                .map(protocol::getDataChannel).boxed().toList());

        return Collections.unmodifiableList(channelIndices);
    }

    @Override
    public List<DecodedMessage> decode(Double[] sampleTime, Map<Integer, Byte[]> sampleValues, int startIndex, int endIndex) {

        List<DecodedMessage> decodedMessages = new ArrayList<>();

        final int N_DATA_CHANNELS = this.getProtocol().getProtocolOptions().getDataChannelCount();

        byte[] currValues = new byte[(N_DATA_CHANNELS- 1) / Byte.SIZE + 1];

        for(int index: getRisingEdgeIndices(sampleValues.get(this.getProtocol().getClockChannel()), startIndex, endIndex)) {

            Arrays.fill(currValues, (byte) 0);

            for(int i = 0; i < N_DATA_CHANNELS; i++) {
                currValues[i / Byte.SIZE] |= sampleValues.get(this.getProtocol().getDataChannel(i))[index] << i;
            }

            decodedMessages.add(ParallelEncoderMessage.newBuilder()
                    .addDecodedFrame(ParallelEncoderFrame.from(currValues, N_DATA_CHANNELS))
                    .setStartTime(Timestamp.of(sampleTime[index]))
                    .setEndTime(Timestamp.of(sampleTime[index]))
                    .setStartIndex(index)
                    .setEndIndex(index)
                    .setDataDirection(DataDirection.UNKNOWN).build());
        }
        return decodedMessages;
    }


    /** === Private Functions ------------------------------------------------ **/

    ParallelEncoderDecoder(ParallelEncoder protocol) {super(protocol);}

    @Override
    protected ParallelEncoder getProtocol() {return (ParallelEncoder) super.getProtocol();}
}