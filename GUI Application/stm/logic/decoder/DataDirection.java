package decoder;

public enum DataDirection {

    SEND("Send"),
    RECEIVE("Receive"),
    UNKNOWN("Unknown");


    /** === Interface Functions ---------------------------------------------- **/

    /**
     * @return A human-readable string representation of this data direction.
     */
    @Override
    public String toString() {return this.value;}


    /** === Private Functions ------------------------------------------------ **/

    /**
     * Creates a data direction enum with the specified string as it's readable value.
     *
     * @param value The string representation of the data direction.
     */
    DataDirection(String value) {this.value = value;}


    /** === Instance Variables ----------------------------------------------- **/

    private final String value;
}
