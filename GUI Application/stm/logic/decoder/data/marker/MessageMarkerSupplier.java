package decoder.data.marker;

public interface MessageMarkerSupplier<T extends MessageMarker> {

    /**
     * Constructs a message marker from the specified start and stop (or another start) signals.
     *
     * @param start The start signal as a {@link Signal} object.
     * @param startOrStop The stop signal (or the next first start signal) as a {@link Signal} object.
     *
     * @return The message marker as a {@link MessageMarker} object.
     */
    T from(Signal start, Signal startOrStop);
}
