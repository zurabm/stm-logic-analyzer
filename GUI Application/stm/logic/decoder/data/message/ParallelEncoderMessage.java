package decoder.data.message;

import decoder.DataDirection;
import decoder.data.frame.DecodedFrame;
import protocols.protocol.ProtocolType;
import utility.Timestamp;

import java.util.Collection;

public class ParallelEncoderMessage extends DecodedMessage {

    /**
     * Builder class for the {@link ParallelEncoderMessage}.
     */
    public static class ParallelEncoderMessageBuilder extends DecodedMessageBuilder {

        @Override
        public ParallelEncoderMessage build() {
            return (ParallelEncoderMessage) super.build();
        }

        @Override
        public ParallelEncoderMessageBuilder addDecodedFrame(DecodedFrame frame) {
            return (ParallelEncoderMessageBuilder) super.addDecodedFrame(frame);
        }

        @Override
        public <T extends DecodedFrame> ParallelEncoderMessageBuilder addDecodedFrames(Collection<T> frames) {
            return (ParallelEncoderMessageBuilder) super.addDecodedFrames(frames);
        }

        @Override
        public ParallelEncoderMessageBuilder setStartTime(Timestamp startTime) {
            return (ParallelEncoderMessageBuilder) super.setStartTime(startTime);
        }

        @Override
        public ParallelEncoderMessageBuilder setEndTime(Timestamp endTime) {
            return (ParallelEncoderMessageBuilder) super.setEndTime(endTime);
        }

        @Override
        public ParallelEncoderMessageBuilder setStartIndex(int startIndex) {
            return (ParallelEncoderMessageBuilder) super.setStartIndex(startIndex);
        }

        @Override
        public ParallelEncoderMessageBuilder setEndIndex(int endIndex) {
            return (ParallelEncoderMessageBuilder) super.setEndIndex(endIndex);
        }

        @Override
        public ParallelEncoderMessageBuilder setDataDirection(DataDirection direction) {
            return (ParallelEncoderMessageBuilder) super.setDataDirection(direction);
        }

        @Override
        protected DecodedMessage buildDecodedMessage() {
            return new ParallelEncoderMessage();
        }

        private ParallelEncoderMessageBuilder() {}
    }

    /**
     * Creates a blank {@link ParallelEncoderMessage} object.
     */
    private ParallelEncoderMessage() {}

    /**
     * @return A builder for this {@link ParallelEncoderMessage} as a {@link ParallelEncoderMessageBuilder}.
     */
    public static ParallelEncoderMessageBuilder newBuilder() {return new ParallelEncoderMessageBuilder();}

    @Override
    public ProtocolType getProtocol() {return ProtocolType.PARALLEL_ENCODER;}
}
