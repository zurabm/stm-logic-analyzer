package decoder.data.message;

import decoder.DataDirection;
import decoder.data.frame.DecodedFrame;
import protocols.protocol.ProtocolType;
import utility.Timestamp;

import java.util.Collection;

public class I2CMessage extends DecodedMessage {

    /** === Enum Definitions ------------------------------------------------- **/

    public enum StopType {
        STOP("Full Stop (Bus released)"),
        REPEATED_START("Repeated Start (Bus not released)");

        StopType(String value) {this.value = value;}

        @Override
        public String toString() {return this.value;}

        private final String value;
    }


    /** === Builder Class Definition ----------------------------------------- **/

    /**
     * Builder class for the {@link I2CMessage}.
     */
    public static class I2CMessageBuilder extends DecodedMessageBuilder {

        /**
         * Sets the stop signal type for this {@link I2CMessage}.
         *
         * @param stopType The stop signal type as a {@link StopType}.
         *
         * @return This {@link I2CMessageBuilder} object.
         */
        public I2CMessageBuilder setStopType(StopType stopType) {
            this.stopType = stopType;
            return this;
        }

        @Override
        public I2CMessage build() {
            return (I2CMessage) super.build();
        }

        @Override
        public I2CMessageBuilder addDecodedFrame(DecodedFrame frame) {
            return (I2CMessageBuilder) super.addDecodedFrame(frame);
        }

        @Override
        public <T extends DecodedFrame> I2CMessageBuilder addDecodedFrames(Collection<T> frames) {
            return (I2CMessageBuilder) super.addDecodedFrames(frames);
        }

        @Override
        public I2CMessageBuilder setStartTime(Timestamp startTime) {
            return (I2CMessageBuilder) super.setStartTime(startTime);
        }

        @Override
        public I2CMessageBuilder setEndTime(Timestamp endTime) {
            return (I2CMessageBuilder) super.setEndTime(endTime);
        }

        @Override
        public I2CMessageBuilder setStartIndex(int startIndex) {
            return (I2CMessageBuilder) super.setStartIndex(startIndex);
        }

        @Override
        public I2CMessageBuilder setEndIndex(int endIndex) {
            return (I2CMessageBuilder) super.setEndIndex(endIndex);
        }

        @Override
        public I2CMessageBuilder setDataDirection(DataDirection direction) {
            return (I2CMessageBuilder) super.setDataDirection(direction);
        }

        @Override
        protected DecodedMessage buildDecodedMessage() {

            I2CMessage message = new I2CMessage();
            message.stopType = this.stopType;

            return message;
        }

        private I2CMessageBuilder() {}


        /** === Instance Variables ----------------------------------------------- **/

        private StopType stopType;
    }


    /** === Interface Functions ---------------------------------------------- **/

    /**
     * @return A builder for this {@link I2CMessage} as a {@link I2CMessageBuilder}.
     */
    public static I2CMessageBuilder newBuilder() {return new I2CMessageBuilder();}

    /**
     * @return The stop type specified for this {@link I2CMessage} as a {@link StopType}.
     */
    public StopType getStopType() {return this.stopType;}

    @Override
    public ProtocolType getProtocol() {return ProtocolType.I2C;}

    @Override
    public String toString() {return super.toString() + "\nStop Type: " + this.getStopType();}


    /** === Private Functions ------------------------------------------------ **/

    /**
     * Creates a blank {@link I2CMessage} object.
     */
    private I2CMessage() {}


    /** === Instance Variables ----------------------------------------------- **/

    private StopType stopType;
}
