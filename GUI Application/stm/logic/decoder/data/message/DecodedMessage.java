package decoder.data.message;

import decoder.DataDirection;
import decoder.data.frame.DecodedFrame;
import protocols.protocol.ProtocolType;
import utility.Timestamp;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public abstract class DecodedMessage {

    private static final String FORMAT = """
            Direction "%s" (Samples %d:%d)
            %s Decoded Message(%s - %s), %d Frame(s):
            %s""";

    private static final String HTML_FORMAT = """
            <h2>
                Direction "%s" (Samples %d:%d)<br>
                %s Decoded Message(%s - %s)<br>
            </h2>
            <h3>%d Frame(s):</h3>
            <p>
                %s
            </p>""";

    /** === Builder Class Definition ----------------------------------------- **/

    /**
     * Builder class for {@link DecodedMessage}.
     */
    public abstract static class DecodedMessageBuilder {

        /**
         * @return The {@link DecodedMessage} built from this {@link DecodedMessageBuilder}.
         */
        public DecodedMessage build() {
            return this.buildBase(this.buildDecodedMessage());
        }

        /**
         * Adds the specified {@link DecodedFrame} to the list of frames that will be present in the
         * built {@link DecodedMessage}.
         *
         * @param frame The data frame as a {@link DecodedFrame}.
         *
         * @return This {@link DecodedMessageBuilder} object.
         */
        public DecodedMessageBuilder addDecodedFrame(DecodedFrame frame) {
            this.decodedFrames.add(frame);
            return this;
        }

        /**
         * Adds the specified frames to the list of frames that will be present in the
         * built {@link DecodedMessage}.
         *
         * @param frames The data frames as a collection of {@link DecodedFrame} objects.
         *
         * @return This {@link DecodedMessageBuilder} object.
         */
        public <T extends DecodedFrame> DecodedMessageBuilder addDecodedFrames(Collection<T> frames) {
            this.decodedFrames.addAll(frames);
            return this;
        }

        /**
         * Sets the start time for the {@link DecodedMessage}.
         *
         * @param startTime The start time as a {@link Timestamp}.
         *
         * @return This {@link DecodedMessageBuilder} object.
         */
        public DecodedMessageBuilder setStartTime(Timestamp startTime) {
            this.startTime = startTime;
            return this;
        }

        /**
         * Sets the end time for the {@link DecodedMessage}.
         *
         * @param endTime The end time as a {@link Timestamp}.
         *
         * @return This {@link DecodedMessageBuilder} object.
         */
        public DecodedMessageBuilder setEndTime(Timestamp endTime) {
            this.endTime = endTime;
            return this;
        }

        /**
         * Sets the start sample index for the {@link DecodedMessage}.
         *
         * @param startIndex The start sample index.
         *
         * @return This {@link DecodedMessageBuilder} object.
         */
        public DecodedMessageBuilder setStartIndex(int startIndex) {
            this.startIndex = startIndex;
            return this;
        }

        /**
         * Sets the end sample index for the {@link DecodedMessage}.
         *
         * @param endIndex The end sample index.
         *
         * @return This {@link DecodedMessageBuilder} object.
         */
        public DecodedMessageBuilder setEndIndex(int endIndex) {
            this.endIndex = endIndex;
            return this;
        }

        /**
         * Sets the assumed direction of the data present in the {@link DecodedMessage}.
         *
         * @param direction The direction of the data as a {@link DataDirection}.
         *
         * @return This {@link DecodedMessageBuilder} object.
         */
        public DecodedMessageBuilder setDataDirection(DataDirection direction) {
            this.direction = direction;
            return this;
        }


        /** === Private Functions ------------------------------------------------ **/

        /**
         * The return value of this abstract method will be used to create a {@link DecodedMessage} object. Only
         * sub-class specific initialization needs to be performed on the {@link DecodedMessage}.
         *
         * @return An initialized instance of this {@link DecodedMessage} object.
         */
        protected abstract DecodedMessage buildDecodedMessage();

        /**
         * Creates a blank {@link DecodedMessageBuilder} object.
         */
        protected DecodedMessageBuilder() {
            this.decodedFrames = new ArrayList<>();
        }

        /**
         * Sets the base parameters for the specified {@link DecodedMessage} object.
         *
         * @param decodedMessage The data for which the parameters should be set as a {@link DecodedMessage} object.
         */
        private DecodedMessage buildBase(DecodedMessage decodedMessage) {

            Objects.requireNonNull(this.startTime, "The start timestamp cannot be null.");
            Objects.requireNonNull(this.endTime, "The end timestamp cannot be null.");
            Objects.requireNonNull(this.direction, "The data direction cannot be null.");

            assert this.decodedFrames.size() > 0: "The message should contain at least one data frame.";
            assert this.startTime.compareTo(this.endTime) <= 0: "The start timestamp cannot be more than the end timestamp.";
            assert this.startIndex <= this.endIndex: "The index of the first sample cannot be more than that of the last.";

            decodedMessage.decodedFrames = this.decodedFrames;
            decodedMessage.startTime = this.startTime;
            decodedMessage.endTime = this.endTime;
            decodedMessage.startIndex = this.startIndex;
            decodedMessage.endIndex = this.endIndex;
            decodedMessage.direction = this.direction;

            return decodedMessage;
        }


        /** === Instance Variables ----------------------------------------------- **/

        private final List<DecodedFrame> decodedFrames;
        private Timestamp startTime;
        private Timestamp endTime;
        private int startIndex;
        private int endIndex;
        private DataDirection direction;
    }

    /** === Interface Functions ---------------------------------------------- **/

    /**
     * @return The protocol which was used to decoded this data as a {@link ProtocolType}.
     */
    public abstract ProtocolType getProtocol();

    /**
     * @return The decoded data in a human readable format.
     */
    @Override
    public String toString() {

        return String.format(FORMAT, this.getDirection(), this.getStartIndex(), this.getEndIndex(),
                this.getProtocol(), this.getStartTime(), this.getEndTime(), this.getDataFrameCount(),
                this.getDataFrames().stream().map(DecodedFrame::toString).collect(Collectors.joining("\n")));
    }

    /**
     * @return The decoded data in HTML format.
     */
    public String toHTMLString() {

        return String.format(HTML_FORMAT, this.getDirection(), this.getStartIndex(), this.getEndIndex(),
                this.getProtocol(), this.getStartTime(), this.getEndTime(), this.getDataFrameCount(),
                this.getDataFrames().stream().map(DecodedFrame::toHTMLString).collect(Collectors.joining("<br>")));
    }

    /**
     * @return The decoded data stored in this {@link DecodedMessage} as an array of {@link DecodedFrame} objects.
     */
    public List<? extends DecodedFrame> getDataFrames() {return this.decodedFrames;}

    /**
     * @return The number of frames in this decoded message.
     */
    public int getDataFrameCount() {return this.decodedFrames.size();}

    /**
     * @return The {@link Timestamp} of the first decoded sample.
     */
    public Timestamp getStartTime() {return this.startTime;}

    /**
     * @return The {@link Timestamp} of the last decoded sample.
     */
    public Timestamp getEndTime() {return this.endTime;}

    /**
     * @return The index of the first decoded sample.
     */
    public int getStartIndex() {return this.startIndex;}

    /**
     * @return The index of the last decoded sample.
     */
    public int getEndIndex() {return this.endIndex;}

    /**
     * @return The direction of the decoded data.
     */
    public DataDirection getDirection() {return this.direction;}


    /** === Private Functions ------------------------------------------------ **/

    /**
     * Creates a blank {@link DecodedMessage} object.
     */
    protected DecodedMessage() {}


    /** === Instance Variables ----------------------------------------------- **/

    private List<DecodedFrame> decodedFrames;
    private Timestamp startTime;
    private Timestamp endTime;
    private int startIndex;
    private int endIndex;
    private DataDirection direction;
}
