package decoder.data.frame;

import decoder.DataDirection;

import java.math.BigInteger;
import java.nio.ByteOrder;

import static protocols.protocol.I2C.N_ACK_BITS;
import static protocols.protocol.I2C.N_DIRECTION_BITS;

public class I2CFrame extends DecodedFrame {

    private enum FrameType {ADDRESS_FRAME, DATA_FRAME}


    /** === Interface Functions ---------------------------------------------- **/

    /**
     * Creates a {@link I2CFrame} from the specified byte array. The frame is interpreted as a raw data frame.
     *
     * @param data The data to be wrapped as a byte array.
     * @param acked If true the data frame was acknowledged, otherwise it wasn't.
     *
     * @return A {@link I2CFrame} containing the specified data.
     */
    public static I2CFrame asDataFrame(byte[] data, boolean acked) {
        return new I2CFrame(data, acked, FrameType.DATA_FRAME, DataDirection.UNKNOWN, Byte.SIZE);
    }

    /**
     * Creates a {@link I2CFrame} from the specified byte array. The frame is interpreted as the initial data frame.
     *
     * @param data The data to be wrapped as a byte array.
     * @param acked If true the data frame was acknowledged, otherwise it wasn't.
     * @param direction The direction specified in the address frame.
     * @param nAddressBits The number of bits containing the address
     *
     * @return A {@link I2CFrame} containing the specified data.
     */
    public static I2CFrame asAddressFrame(byte[] data, boolean acked, DataDirection direction, int nAddressBits) {
        return new I2CFrame(data, acked, FrameType.ADDRESS_FRAME, direction, nAddressBits + N_DIRECTION_BITS + N_ACK_BITS);
    }

    /**
     * @return The byte ordering of the data in this {@link DecodedFrame}.
     */
    @Override
    public ByteOrder getByteOrdering() {return ByteOrder.BIG_ENDIAN;}

    /**
     * @return THe number of bits that were decoded for this {@link DecodedFrame}.
     */
    @Override
    public int getDecodedBitCount() {return this.nDecodedBits;}

    /**
     * @return A human readable string representation of this {@link DecodedFrame}.
     */
    @Override
    public String toString() {

        String asString;
        String data = new BigInteger(1, this.getData()).toString(10);

        switch (this.frameType) {
            case DATA_FRAME -> asString = String.format("%s (%s)", data, this.acked ? "ACKED": "NACKED");
            case ADDRESS_FRAME -> asString = String.format("Address: %s - %s (%s)",
                    data, this.getDirection(), this.acked ? "ACKED": "NACKED");

            default -> throw new RuntimeException("Unknown frame type " + this.frameType + ".");
        }

        return asString;
    }

    /**
     * @return A human readable string representation of this {@link DecodedFrame} in HTML format.
     */
    @Override
    public String toHTMLString() {
        return this.toString();
    }

    /**
     * Returns the directions specified in this {@link I2CFrame}. The direction will be unknown for raw Data Frames.
     *
     * @return The direction as a {@link DataDirection} object. (i.e. either Read, Write or Unknown).
     */
    public DataDirection getDirection() {return this.direction;}


    /** === Private Methods -------------------------------------------------- **/

    private I2CFrame(byte[] data, boolean acked, FrameType frameType, DataDirection direction, int nDecodedBits) {

        super(data);

        this.acked = acked;
        this.frameType = frameType;
        this.direction = direction;
        this.nDecodedBits = nDecodedBits;
    }


    /** === Instance Variables ----------------------------------------------- **/

    private final boolean acked;
    private final FrameType frameType;
    private final DataDirection direction;
    private final int nDecodedBits;
}
