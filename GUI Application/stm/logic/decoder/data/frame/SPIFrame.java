package decoder.data.frame;

import java.math.BigInteger;
import java.nio.ByteOrder;

public class SPIFrame extends DecodedFrame {

    /** === Interface Functions ---------------------------------------------- **/

    /**
     * @return The byte ordering of the data in this {@link DecodedFrame}.
     */
    @Override
    public ByteOrder getByteOrdering() {return ByteOrder.BIG_ENDIAN;}

    /**
     * @return THe number of bits that were decoded for this {@link DecodedFrame}.
     */
    @Override
    public int getDecodedBitCount() {
        return 0;
    }

    /**
     * @return A human readable string representation of this {@link DecodedFrame}.
     */
    @Override
    public String toString() {
        return "Data: " + new BigInteger(1, this.getData()).toString(10);
    }

    /**
     * @return A human readable string representation of this {@link DecodedFrame} in HTML format.
     */
    @Override
    public String toHTMLString() {
        return this.toString();
    }

    /**
     * Creates a new {@link SPIFrame} from the specified data.
     *
     * @param data The data as a raw byte array.
     *
     * @return The data frame as an {@link SPIFrame}.
     */
    public static SPIFrame from(byte[] data) {return new SPIFrame(data);}


    /** === Private Methods -------------------------------------------------- **/

    protected SPIFrame(byte[] data) {super(data);}
}
