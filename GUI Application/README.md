# GUI Application for the Digital Logic Analyzer

## How to use

1. Compile the project and run main from the source file **/stm/main/GUIMain.java**.
2. Once the application is started connect a compatible STM32 Microcontroller to your computer via USB.
   Some Notes:
    * Make sure that the microcontroller is correctly programmed/configured, check out the directory with the STMCubeIDE project for more information.
    * Make sure you're using the correct USB port on the microcontroller. Usually there will be at least two USB ports,
     but one is used for programming/debugging the device and the other can be used by the device to act as a USB Device.

3. Open the settings menu to make sure the correct Virtual COM port is selected. The port for your STM, acting as a USB Device, will
 probably have "Virtual COM Port" mentioned in the name.
4. Configure the sampling parameters/channels and everything else you need.
5. Press the start button and you're set.

___

If you get an error message at any point, the issue is most likely that either the microcontroller is not programmed with the latest source, or the jSerialComm can't access the USB ports on your computer.
