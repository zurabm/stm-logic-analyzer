# STM Logic Analyzer
## How to use

You're gonna need this project to program your STM32 microcontroller. 

1. Open the project with your IDE.
2. Open **STM Logic Analyzer.ioc** from the editor.
3. From the toolbar in the top-left corner of the screen, click on the Code Generation button to regenerate the code. 

    ![image-1.png](./Assets/IDE Toolbar.png)

4. Check the header file **./USB_DEVICE/App/usbd_cdc_if.h** and make sure that the buffer sizes are defined appropriately for your microcontroller. 

    ![image-2.png](./Assets/usbd_cdc_if.png)

    If they're too big, you'll get an error and won't be able to upload the code. Also, remove any other defines for these buffers if the Code Generation tool created any.

5. Upload the code to your STM and you're done.

___

The configuration file may need to be modified for some microcontrollers, but it should definitely work for at least the STM32F407G Discovery Board.
